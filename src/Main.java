import board.*;
import solver.KnightsTour;
import verifier.Verifier;

public class Main {

	public static void main(String[] args) {	
		
		final boolean bShowConvbacktrack = true; // <true> to show the conventional back-tracking without Warndorf's rule
		
		Chessboard MyBoard = new Chessboard(8);
		
		KnightsTour KnightsTour = new KnightsTour(MyBoard);
		System.out.println("Knight's tour problem (Springerproblem)..\n");
		
		boolean bResult = false;
		long lStart, lStop, lElapsedTime;
		
		
		/*
		 * Backtracking (brute force) using Warndorf's rule to optimize (decrease) runtime
		 */
		lStart = System.currentTimeMillis(); // [ms]
		bResult = KnightsTour.findPath_Warndorf(0,0); // Backtracking with Warndorf's rule
		lStop = System.currentTimeMillis();
		lElapsedTime = lStop - lStart;		
		System.out.println("Backtracking with Warndorf. Time elapsed: " + lElapsedTime + " [ms]");		
		if (bResult) {			
			System.out.print("Path is found:");
			KnightsTour.printPath();
		}
		else {
			System.out.println("No path.");
		}		
		MyBoard.clearBoard();
		
		/*
		 * Verification :: Backtracking with Warndorf's rule
		 */
		Verifier MyVerifier = new Verifier();
		if (MyVerifier.verify_knightstour(MyBoard, KnightsTour.getList())) {
			System.out.println("Verification: Found path is correct.");
		}
		else {
			System.out.println("Verification: Path not correct.");
		}		
		MyBoard.printBoard();
		
		System.out.println();
		KnightsTour.resetAll();
		
		
		/*
		 * Conventional backtracking (brute force), without optimization by Warndorf's rule
		 */
		if (!bShowConvbacktrack) return;
		
		System.out.println("In the following, we are going to demonstrate the conventional back-tracking "
				+ "without Warndorf's.\nIn many cases, the Java program will break out during recursion. "
				+ "And an StackOverFlowError exception will raise.\nThe reason is because the current stack size is "
				+ "not enough for to much recursive calls. To solve this, we have\nto increase the stack size, as follows:\n"
				+ "Set JVM argument \"-Xss9999m\" to increase stack size.\n" 
				+ "	 # java -Xss9999m Main.class\n" 
				+ "In Eclipse, go to [Run Configurations] => [(x)=Arguments] => [VM arguments] => -Xss9999m.\n"
				+ "Hence, the stack limit will be set up to 9999 Mb. It should be enough in the most cases.");
		lStart = System.currentTimeMillis(); // [ms]
		bResult = KnightsTour.findPath(0,0); // Backtracking
		lStop = System.currentTimeMillis();
		lElapsedTime = lStop - lStart;
		System.out.println("\nConventional backtracking. Time elapsed: " + lElapsedTime + " [ms]");		
		if (bResult) {			
			System.out.print("Path is found:");
			KnightsTour.printPath();
		}
		else {
			System.out.println("No path.");
		}		
		MyBoard.clearBoard();
		
		/*
		 * Verification :: Conventional backtracking
		 */
		if (MyVerifier.verify_knightstour(MyBoard, KnightsTour.getList())) {
			System.out.println("Verification: Found path is correct.");
		}
		else {
			System.out.println("Verification: Path not correct.");
		}		
		MyBoard.printBoard();
		
	}
	
}
