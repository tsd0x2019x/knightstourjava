package utils;
import java.util.ArrayList;
import java.util.List;

public class PossibleMovesFromField {

	// Current field where the knight (Springer) is currently in.
	private Field m_currentField;

	// List of all possible next moves from current field <m_currentField>.
	private List<Field> m_possibleMoves;
	
	public PossibleMovesFromField(Field currentField) {
		m_currentField = currentField;
		m_possibleMoves = new ArrayList<Field>();
	}
	
	/**
	 * Appends the specified field (element) at the end of this list.
	 * This list contains the next possible moves of the knight (Springer) from the specified field.
	 */
	public void addElement(Field f) {
		m_possibleMoves.add(f);
	}
	
	/**
	 * Appends the specified field (element) with <iRow> and <iCol> at the end of this list.
	 * This list contains the next possible moves of the knight (Springer) from the specified field.
	 * @param iRow : Row index of field to start from
	 * @param iCol : Column index of field to start from
	 */
	public void addElement(int iRow, int iCol) {
		m_possibleMoves.add(new Field(iRow, iCol));
	}
	
	/**
	 * Provides the current field where the knight (Springer) is currently in.
	 */
	public Field getCurrentField() {
		return m_currentField;
	}
	
	/**
	 * Provides the (reference of) element with specified <index> of this list.
	 * @param index : Index of the field to be provided  
	 */
	public Field getElement(int index) {
		return m_possibleMoves.get(index);
	}
	
	/**
	 * Return the size of this list (i.e. number of next possible moves, starting from <m_currenField>)
	 * @return Size of this list (i.e. number of next possible moves, starting from <m_currenField>)
	 */
	public int getSize() {
		return m_possibleMoves.size();
	}
	
	/**
	 * Pop the last element of this list (i.e. removes the last element and provide it).
	 * If the list is empty (i.e. list.size() == 0), then <i>null</i> will be returned.
	 * @return Provide the last element that was removed from the list previously. If the list is empty, <i>null</i> is returned.
	 */
	public Field pop_back() {
		if (m_possibleMoves.size() == 0) return null;
		return m_possibleMoves.remove(m_possibleMoves.size()-1);
	}
	
	/**
	 * Removes the element at <index> from this list. The index <index> is in range [0,size()-1].
	 * Returns the removed element.
	 * @return Field that is removed from this list previously.
	 */
	public Field removeElement(int index) {
		return m_possibleMoves.remove(index);
	}

}
