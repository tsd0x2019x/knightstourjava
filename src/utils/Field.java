package utils;

public class Field {

	public int row, col;
	
	public Field(int row, int col) {
		this.row = row;
		this.col = col;
	}
	
	public Field() {
		this(0,0);
	}
	
}
