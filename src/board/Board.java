package board;

public class Board {

	// Member variables
	private int m_iHeight, m_iWidth;	
	private boolean m_arrBoard[][];

	private void initialize(int iHeight, int iWidth) {
		m_iHeight = iHeight;
		m_iWidth = iWidth;
		// Initialize board's fields
		m_arrBoard = new boolean[iHeight][iWidth];
		for(int i = 0; i < iHeight; i++)
			for(int j = 0; j < iWidth; j++)
				m_arrBoard[i][j] = false; // empty field
	}
	
	// Constructor
	public Board(int iHeight, int iWidth) {
		initialize(iHeight, iWidth);
	}

	public int getHeight() {
		return m_iHeight;
	}
	
	public int getWidth() {
		return m_iWidth;
	}
	
	public boolean isOutOfBoard(int iRow, int iCol) {
		return (iRow < 0 || iRow >= m_iHeight || iCol < 0 || iCol >= m_iWidth) ? true : false;
	}
	
	public boolean isEmptyField(int iRow, int iCol) {
		return isOutOfBoard(iRow, iCol) ? false : (m_arrBoard[iRow][iCol]) ? false : true; 
	}
	
	public boolean isValidField(int iRow, int iCol) {
		return isEmptyField(iRow, iCol);
	}
	
	public void printBoard() {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < m_iHeight; i++) {
			for(int j= 0; j < m_iWidth; j++) {
				sb.append(" " + (m_arrBoard[i][j] ? "\u25A0" : "\u25A1"));
			}
			sb.append("\n");
		}
		System.out.println(sb.toString());
	}
	
	public void resetBoard() {
		for(int i = 0; i < m_iHeight; i++) {
			for(int j = 0; j < m_iWidth; j++) {
				m_arrBoard[i][j] = false;
			}
		}
	}
	
	public void clearBoard() {
		this.resetBoard();
	}
	
	/**
	 * Set the specified field with <i>iRow</i> and <i>iCol</i> to true, i.e. this field is visited.
	 * @param iRow
	 * @param iCol
	 */
	public void setField(int iRow, int iCol) {
		m_arrBoard[iRow][iCol] = true;
	}
	
	public void unsetField(int iRow, int iCol) {
		m_arrBoard[iRow][iCol] = false;
	}
	
}
