package board;

public class Chessboard extends Board {

	private int m_iBoardSize;
	
	public Chessboard(int iBoardSize) {
		super(iBoardSize,iBoardSize);
		m_iBoardSize = iBoardSize;
	}
	
	public Chessboard() { // 8x8 = 64 fields
		this(8);
	}

	public int getSize() {
		return m_iBoardSize;
	}
		
}
