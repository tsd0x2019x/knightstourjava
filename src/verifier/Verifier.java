package verifier;

import java.util.List;
import board.*;
import utils.*;

/**
 * Verifies the written code in package <i>solver</i>.
 */
public class Verifier {

	public Verifier() {}
	
	public boolean verify_knightstour(Board board, List<PossibleMovesFromField> lst) {
		int iListSize = lst.size();
		if (iListSize < (board.getHeight() * board.getWidth())) return false;
		board.clearBoard();
		for(int i = 0; i < iListSize; i++) {
			Field f = lst.get(i).getCurrentField();
			if (!board.isEmptyField(f.row, f.col)) {
				return false;
			}
			else {
				board.setField(f.row, f.col);
			}
		}
		return true;
	}
}
