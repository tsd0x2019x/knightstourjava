package solver;
import java.util.ArrayList;
import java.util.List;

import board.Chessboard;
import utils.Field;
import utils.PossibleMovesFromField;

/**
 * This program demonstrates and solves the Knight's tour problem (Springerproblem),
 * using backtracking (brute force) with/without Warndorf's rule.
 */
public class KnightsTour {
	
	private static final boolean M_DEBUG = false;
	
	/**
	 *  Chess board
	 */
	private Chessboard m_board;
	/**
	 *  This list contains many lists of possible next moves.
	 */
	private List<PossibleMovesFromField> m_movesList;
	
	public KnightsTour(Chessboard board) {
		m_board = board;
		m_movesList = new ArrayList<PossibleMovesFromField>();
		m_board.resetBoard();
	}	
	
	/**
	 * Check if the specified field with <i>iRow</i> and <i>iCol</i> is valid (i.e. free and within board).
	 * @param iRow : Row index of the specified field to be checked. 
	 * @param iCol : Column index of the specified field to be checked.
	 * @return <i>true</i> : The specified field is valid (i.e. free and within board)<br /><i>false</i> : Otherwise
	 */
	private boolean isValidField(int iRow, int iCol) {
		return m_board.isValidField(iRow, iCol);
	}
	
	/**
	 * Pop the last element of list <i>m_movesList</i> (i.e. remove the last element from list and provide this element).
	 * @return The last element (from list <i>m_movesList</i>) that was removed previously.
	 */
	private PossibleMovesFromField pop_back() {
		int iSize = m_movesList.size();
		if (iSize == 0) return null;
		return (PossibleMovesFromField) m_movesList.remove(iSize-1);
	}
	
	/**
	 * Provides number of all next possible moves starting from the specified field with <i>iRow</i> and <i>iCol</i>.
	 * @param iRow : Row index of field to start from
	 * @param iCol : Column index of field to start from
	 * @return Number of all next possible moves starting from the specified field with <i>iRow</i> and <i>iCol</i>.
	 */
	private int getNumberOfNextMoves(int iRow, int iCol) {
		int iNumber = 0;		
		// 1st possible next move
		if (isValidField(iRow-2, iCol-1)) iNumber++;
		// 2nd possible next move
		if (isValidField(iRow-1, iCol-2)) iNumber++;
		// 3rd possible next move
		if (isValidField(iRow+1, iCol-2)) iNumber++;
		// 4th possible next move
		if (isValidField(iRow+2, iCol-1)) iNumber++;
		// 5th possible next move
		if (isValidField(iRow+2, iCol+1)) iNumber++;
		// 6th possible next move
		if (isValidField(iRow+1, iCol+2)) iNumber++;
		// 7th possible next move
		if (isValidField(iRow-1, iCol+2)) iNumber++;
		// 8th possible next move
		if (isValidField(iRow-2, iCol+1)) iNumber++;
		
		return iNumber;
	}
	
	/**
	 * Find the (eight) possible next valid/free fields to jump to from the current field (<i>iRow</i>,<i>iCol</i>).
	 */
	private PossibleMovesFromField searchPossibleNextMovesFromField(int iRow, int iCol, PossibleMovesFromField pm) {
		// 1st possible next move
		if (isValidField(iRow-2, iCol-1)) pm.addElement(iRow-2, iCol-1);	
		// 2nd possible next move
		if (isValidField(iRow-1, iCol-2)) pm.addElement(iRow-1, iCol-2);
		// 3rd possible next move
		if (isValidField(iRow+1, iCol-2)) pm.addElement(iRow+1, iCol-2);
		// 4th possible next move
		if (isValidField(iRow+2, iCol-1)) pm.addElement(iRow+2, iCol-1);
		// 5th possible next move
		if (isValidField(iRow+2, iCol+1)) pm.addElement(iRow+2, iCol+1);
		// 6th possible next move
		if (isValidField(iRow+1, iCol+2)) pm.addElement(iRow+1, iCol+2);
		// 7th possible next move
		if (isValidField(iRow-1, iCol+2)) pm.addElement(iRow-1, iCol+2);
		// 8th possible next move
		if (isValidField(iRow-2, iCol+1)) pm.addElement(iRow-2, iCol+1);
		
		return pm;
	}
	
	/**
	 * Backtracking with Warndorf's rule to optimize (decrease) the runtime.
	 */
	private boolean back_tracking_Warndorf() {
		if (m_movesList.size() == 0) {
			return false; // No path !
		}
		// m_movesList.size() > 0
		// Dead-end => go back to previous move
		PossibleMovesFromField pm = this.pop_back(); // m_movesList		
		int pmSize = pm.getSize();		
		if (pmSize > 0) {
			int iNum = 9;
			int iIndex = 0; //int iIndex = pmSize-1;
			int i = 0;
			Field f = null;
			while(i < pmSize) {
				f = pm.getElement(i);
				int iTmp = this.getNumberOfNextMoves(f.row, f.col);
				if (iTmp != 0 && iTmp < iNum) {
					iNum = iTmp;
					iIndex = i;
				}
				++i;
			}
			f = pm.removeElement(iIndex);
			return findPath_Warndorf(f.row, f.col);
		}
		else { // pm.getSize() == 0
			m_board.unsetField(pm.getCurrentField().row, pm.getCurrentField().col);
			return back_tracking_Warndorf();
		}
	}
	
	
	/**
	 * Back tracking (recursively)
	 */
	private boolean back_tracking() {
		if (m_movesList.size() == 0) {
			return false; // No path !
		}
		// m_movesList.size() > 0
		PossibleMovesFromField pm = this.pop_back();
		if (pm.getSize() > 0) {
			Field f = pm.pop_back();
			m_movesList.add(pm);
			return findPath(f.row, f.col);
		}
		else { // pm.getSize() == 0
			m_board.unsetField(pm.getCurrentField().row, pm.getCurrentField().col);
			return back_tracking();
		}
	}
	
	/**
	 * Find path recursively for the knight's tour problem (Springerproblem), using back-tracking (brute-force).
	 * Additionally using the Warndorf's rule to optimize (decrease) the runtime.
	 * @param iRow : Row index of field to start from
	 * @param iCol : Column index of field to start from
	 * @return <i>true</i>: A path for Knight's tour problem (Springerproblem) is found.<br/><i>false</i>: No path can be found.
	 */
	public boolean findPath_Warndorf(int iRow, int iCol) {
		
		Field currentField = new Field(iRow, iCol); // current field
		PossibleMovesFromField pm = new PossibleMovesFromField(currentField); // possible next fields (moves) from current field
		
		m_board.setField(iRow, iCol);
		if (M_DEBUG) m_board.printBoard();
		
		pm = this.searchPossibleNextMovesFromField(iRow, iCol, pm);		
		int pmSize = pm.getSize();
		
		if (pmSize > 0) {			
			m_board.setField(iRow, iCol);
			int iNum = 8; //int iNum = 9;
			int iIndex = 0; //int iIndex = pmSize-1;
			int i = 0;
			Field f = null;
			while(i < pmSize) {
				f = pm.getElement(i);
				int iTmp = this.getNumberOfNextMoves(f.row, f.col);
				if (iTmp != 0 && iTmp < iNum) {
					iNum = iTmp;
					iIndex = i;
				}
				++i;
			}	
			f = pm.removeElement(iIndex);
			m_movesList.add(pm);
			return findPath_Warndorf(f.row, f.col);
		}
		else { // pm.getSize() == 0
			if (m_movesList.size() == (m_board.getHeight()*m_board.getWidth()-1)) { // Path found !
				m_movesList.add(pm);
				return true; // Path found => Exit
			}
			else { // Dead-end => go back to previous move
				m_board.unsetField(iRow, iCol);
				if (M_DEBUG) m_board.printBoard();
				return back_tracking_Warndorf();
			}
		}
	}
	
	/**
	 * Find path recursively for the knight's tour problem (Springerproblem), using back-tracking (brute-force).
	 * Set JVM argument "-Xss9999m" to increase stack size. Otherwise, there will be StackOverflowError.
	 * 		# java -Xss9999m Main.class
	 * In Eclipse, go to [Run Configurations] => [(x)=Arguments] => [VM arguments] => -Xss9999m.
	 * The stack limit will be set up to 9999 Mb.
	 * @param iRow : Row index of field to start from
	 * @param iCol : Column index of field to start from
	 * @return <i>true</i> : A path for Knight's tour problem (Springerproblem) is found.<br/><i>false</i> : No path can be found.
	 */
	public boolean findPath(int iRow, int iCol) {
		
		Field currentField = new Field(iRow, iCol); // current field
		PossibleMovesFromField pm = new PossibleMovesFromField(currentField); // possible next fields (moves) from current field
		
		pm = this.searchPossibleNextMovesFromField(iRow, iCol, pm);
		m_board.setField(iRow, iCol);
		
		if (pm.getSize() > 0) {
			Field f = pm.pop_back();
			m_movesList.add(pm);
			return findPath(f.row, f.col);
		}
		else { // pm.getSize() == 0
			if (m_movesList.size() == (m_board.getHeight()*m_board.getWidth()-1)) { // Path found !
				m_movesList.add(pm);
				return true; // Path found => Exit
			}
			else { // Dead end => go back to previous move
				m_board.unsetField(iRow, iCol);
				return back_tracking();
			}
		}
	}
	
	/**
	 * Reset (empty) the list and board. Equivalent to method <i>resetAll()</i>.
	 */
	public void clearAll() {
		m_movesList.clear();
		m_board.clearBoard();
	}
	
	/**
	 * Reset (empty) the list and board. Equivalent to method <i>clearAll()</i>.
	 */
	public void resetAll() {
		this.clearAll();
	}
	
	public List<PossibleMovesFromField> getList() {
		return m_movesList;
	}
	
	/**
	 * Prints whole path of Knight's tour (Springerproblem).
	 */
	public void printPath() {
		String sPath = "";
		Field current = null;
		int iSize = m_movesList.size();
		for(int i = 0; i < iSize-1; i++) {
			current = m_movesList.get(i).getCurrentField(); 
			sPath += "(" + current.row + "," + current.col + ")->";
		}
		current = m_movesList.get(iSize-1).getCurrentField();
		sPath += "(" + current.row + "," + current.col + ")";
		System.out.println(sPath);
	}
	
}
